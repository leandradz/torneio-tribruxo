import { Component } from "react";
import "./App.css";
import StudentList from "./components/StudentList";

class App extends Component {
  state = {
    studentList: [],
    listRandomStudent: [],
    houses: [],
  };

  componentDidMount() {
    fetch("https://hp-api.herokuapp.com/api/characters/students")
      .then((response) => response.json())
      .then((response) => this.setState({ studentList: response }));
  }

  randomStudent = (list) => {
    const { studentList, listRandomStudent, houses } = this.state;
    let newStudent =
      studentList[Math.floor(Math.random() * studentList.length)];
    if (
      !listRandomStudent.includes(newStudent) &&
      !houses.includes(newStudent.house)
    ) {
      this.setState({
        listRandomStudent: [...listRandomStudent, newStudent],
        houses: [...houses, newStudent.house],
      });
    } else {
      return this.randomStudent(studentList);
    }
  };

  render() {
    const { studentList, listRandomStudent } = this.state;

    return (
      <div className="App">
        <header className="App-header">
          <div className="cointainer">
            <h1>Torneio Tribruxo</h1>
            <div className="containerButton">
              <button
                onClick={() => this.randomStudent(studentList)}
                disabled={listRandomStudent.length === 3}
              >
                Retirar da Taça
              </button>
              <button
                onClick={() =>
                  this.setState({ listRandomStudent: [], houses: [] })
                }
              >
                Reiniciar
              </button>
            </div>
            <div className="triBruxo">
              <StudentList list={listRandomStudent} />
            </div>
            <div className="triwizardCup"></div>
          </div>
        </header>
      </div>
    );
  }
}

export default App;
