import { Component } from "react";
import Student from "../Student";

class StudentList extends Component {
  render() {
    const { list } = this.props;
    return list.map((item) => {
      return (
        <div>
          <Student name={item.name} img={item.image} house={item.house} />
        </div>
      );
    });
  }
}

export default StudentList;
