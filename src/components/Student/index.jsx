import { Component } from "react";
import "./styles.css";

class Student extends Component {
  render() {
    const { name, img, house } = this.props;
    return (
      <div className="character">
        <img className="picture" src={img} alt={name}></img>
        <h2>{name}</h2>
        <h3>{house}</h3>
      </div>
    );
  }
}

export default Student;
